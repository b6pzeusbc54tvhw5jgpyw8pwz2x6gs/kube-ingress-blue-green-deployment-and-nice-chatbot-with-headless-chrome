resource "google_compute_network" "default" {
  name = "kubetest"
  auto_create_subnetworks = "true"
}


resource "google_compute_firewall" "default" {
  name = "kubetest-allow"
  network = "${google_compute_network.default.name}"

  allow {
    protocol = "tcp"
    ports = ["80", "443", "22"]
  }
  allow {
    protocol = "all"
  }

  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_firewall" "kube" {
  name = "kubetest-allow-kube"
  network = "${google_compute_network.default.name}"
  allow {
    protocol = "all"
  }

  # source_ranges = ["${data.google_compute_subnetwork.kubetest.ip_cidr_range}"]
  source_tags = ["master","worker"]
}

data "google_compute_subnetwork" "kubetest" {
  name   = "kubetest"
  region = "asia-east1"

  depends_on = [
    "google_compute_network.default",
  ]
}

output "subnetwork_asia_east1" {
  value = "${data.google_compute_subnetwork.kubetest.ip_cidr_range}"
}
