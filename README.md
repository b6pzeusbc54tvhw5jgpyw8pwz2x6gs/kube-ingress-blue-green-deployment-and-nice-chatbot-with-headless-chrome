
# blue green deployment using ingress - 잘생긴 챗봇

## 1. terraform apply
https://console.cloud.google.com/compute/instances?project=todo-uc&authuser=2

## 2. create cluster
http://docs.projectcalico.org/v2.2/getting-started/kubernetes/installation/hosted/kubeadm/
```
$ kubectl apply -f http://docs.projectcalico.org/v2.2/getting-started/kubernetes/installation/hosted/kubeadm/1.6/calico.yaml
```

## 3. route 53 setting
https://console.aws.amazon.com/route53/home?region=us-east-1
- gcp-traefik.alfreduc.com
- gcp-kube.alfreduc.com
- gcp-markdown.alfreduc.com
- gcp-mattermost.alfreduc.com
- gcp-registry.alfreduc.com
- stg-gcp-mattermost.alfreduc.com

## 4. kubeadm join in worker


## 5. create traefik
```
$ kubectl create configmap traefik-conf --from-file=traefik.toml -n kube-system
$ kubectl create secret generic traefik-cert --from-file=alfreduc.com.crt --from-file=alfreduc.com.key -n kube-system
$ kubectl apply -f https://raw.githubusercontent.com/containous/traefik/master/examples/k8s/traefik-rbac.yaml
$ kubectl apply -f traefik.yaml
$ kubectl apply -f traefik-ui.yml
```

## 6. create kubernetes dashboard
```
$ kubectl apply -f kube-dashboard.yml
```

## 8. create mattermost
```
$ kubectl create namespace devp collabotration
$ kubectl create configmap mattermost-conf-3.8 --from-file=config.json -n devp
$ kubectl create configmap mattermost-conf-3.9 --from-file=config.json -n devp
$ kubectl apply -f mattermost-postgresql.yml
$ kubectl apply -f mattermost-blue.yml
$ kubectl apply -f mattermost-ingress.yml
// check veresion on your browser
```

## 9. blue-green deployment mattermost using ingress

> mattermost 의 앱 특성상 한번 높은 버전이 db 에 붙으면, 그 이후로는 낮은 버전이 붙을 수 없음.

```
$ kubectl apply -f mattermost-blue.yml

// edit ingress
$ kubectl replace -f mattermost-ingress.yml

// check version
```

## 10. registry
```
$ kubectl create configmap registry-conf --from-file=todo-uc-24e7952fd8c7.json -n kube-system
$ kubectl apply -f registry.yml
```

## 11. bot-moon
// invite link
bot account

```
$ kubectl create secret generic bot-moon-env --from-file=bot-moon-env -n collabotration
$ kubectl apply -f bot-moon.yml
```

- 봇이 말하는 마크다운이 길어질경우 생기는 문제들: test) `massMarkdown1`
 1. 글자제한때문에 여러번에 걸쳐서 말해야하는데 markdown table 도중에 짤릴경우 table 이 깨진다.
 2. 글자제한때문에 여러번에 걸쳐서 말해야하는데 도중에 다른 사람이나 봇이 말 할 수 있다.
 3. 스크롤이 길어져서 채널을 어지럽힌다.
 4. 한번 말하면 정보를 업데이트하기 힘들다 (수정이 가능하지만 구현해줘야함)

## 12. markdown-server / headless-chrome

- 해결책은? test: `massMarkdown2`
 1. 봇이 말하고 싶은 markdown 문자열을 markdown-server 에게 보냄
 2. markdown-server 는 전달받은 markdown 으로 스스로 웹페이지를 만듬
 3. headless-chrome 에게 스스로 만든 웹페이지를 스샷을 찍어달라고 요청
 4. 스샷의 주소와 웹페이지 주소를 봇에게 응답
 5. 봇은 장문의 긴 markdown 대신 스크린샷 이미지 하나를 보여주고 클릭하면 링크로 markdown-server 로 연결시킴.

```
$ kubectl apply -f markdown-server.yml
$ kubectl apply -f headless-chrome.yml
```

test:
```
$ curl -X POST -H "Content-Type: application# Big title \n## 1. small title \n## 2. small title\n ```\nconsole.log( \"hello markdown\" )\n``` "}' https://gcp-markdown.alfreduc.com/api/render/markdown
```

- 봇이 임의로 생성하는 장문의 마크다운을 표시하는 용도로 적합.
- 봇이 임의로 생성하는 마크다운이 아닌 웹페이지로 서비스되고 있는 데이터를 보여주는데도 응용하면 좋음
- ex: http://127.0.0.1:4004/web


## note
- traefik 굉장히 괜찮아 보인다. 특히 web UI
- 네트워크 안에 들어가서 테스트할일 있을때
```
$ kubectl run tmptmp --image=ubuntu:16.04 -it --rm
```

- registry OOMKilled 발생시, node 에 swap memory 를 셋팅해주면되더라

```shell
#!/bin/sh

# https://programmaticponderings.com/2013/12/19/scripting-linux-swap-space/

# size of swapfile in megabytes
swapsize=$1
swapsize=${swapsize:-4096}

# does the swap file already exist?
grep -q "swapfile" /etc/fstab

# if not then create it
if [ $? -ne 0 ]; then
	echo 'swapfile not found. Adding swapfile.'
	fallocate -l ${swapsize}M /swapfile
	chmod 600 /swapfile
	mkswap /swapfile
	swapon /swapfile
	echo '/swapfile none swap defaults 0 0' >> /etc/fstab
else
	echo 'swapfile found. No changes made.'
fi

# output results to terminal
cat /proc/swaps
cat /proc/meminfo | grep Swap
```

## references

- https://gitlab.com/b6pzeusbc54tvhw5jgpyw8pwz2x6gs/markdown-server
- https://hub.docker.com/r/b6pzeusbc54tvhw5jgpyw8pwz2x6gs/headless-chrome/
- about ingress
  - https://kubernetes.io/docs/concepts/services-networking/ingress/
  - https://subicura.com/2017/02/25/container-orchestration-with-docker-swarm.html
  - https://docs.docker.com/engine/swarm/ingress/#publish-a-port-for-a-service

## TODO:

1. --skip-preflight-checks ??
1. ServiceAccount ??
1. terraform provision 시의 로그에서 token 뽑아내는방법이 있을까.. 
1. configmap 과 secret 의 차이
1. kube dashboard 에서의 인증
1. apply / create / replace / edit 차이
1. Q

```
$ kubectl replace -f registry.yml
deployment "kube-registry" replaced
ingress "kube-registry" replaced
The Service "kube-registry" is invalid: spec.clusterIP: Invalid value: "": field is immutable
```
