// Configure the Google Cloud provider
provider "google" {
  credentials = "${file("todo-uc-24e7952fd8c7.json")}"
  project     = "todo-uc"
  region      = "asia-east1"
}

