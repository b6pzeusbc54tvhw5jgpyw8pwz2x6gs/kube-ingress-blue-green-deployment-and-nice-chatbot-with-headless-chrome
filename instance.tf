resource "google_compute_instance" "master" {
  name         = "kube-master"
  machine_type = "n1-standard-1"
  zone         = "asia-east1-a"
  tags = ["master"]
  disk {
    # image = "debian-cloud/debian-8"
    image = "ubuntu-1604-xenial-v20170619a"
    size = 20
    # type = "local-ssd"
    # scratch = true
  }

  network_interface {
    network = "${google_compute_network.default.name}"
    access_config {
      // Ephemeral IP
    }
  }
  metadata {
    foo = "bar"
    "ssh-keys" = "ssohjiro:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDo7fIa/pjvxrBe1wUR+BR2aoXjbZjB8UvJmy9EgdjLN9TOchJVEeimM+Xolr4X0ZEfzMUQq77nMWJAMd2BBUbi/KE4sB2khOdDfC9hncYBjMFc9g+WYJausDexF+PKZX7PvISwEAsOoXlxBovt3l1V6DoHDVaSYReSv2TBL6CaQC9PcLl+soAWkrFjOeSgUnrQsgPUXE+vUj37JY8qkqd/+sNatShTLnNYU9eV6CYpqsUMH3ccy+vBn+55dra6zXAyO40WSZfTB5Q9FwrG5QrLuk8lNr4V5LzrOBBo4Cz0epIEm9xzmMHGzHhP1J61z1qwOuHPAL0A4rUjBknJ5qzD ssohjiro@2012MidMPR"
  }

  service_account {
    scopes = ["userinfo-email", "compute-ro", "storage-ro"]
  }
  connection {
    user = "ssohjiro"
    private_key = "${file("private_key")}"
  }
  provisioner "file" {
    content = <<SCRIPT
#! /bin/bash
curl -fsSL https://get.docker.com/ | sh

sudo apt-get install -y apt-transport-https
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
sudo bash -c "echo deb http://apt.kubernetes.io/ kubernetes-xenial main >> /etc/apt/sources.list.d/kubernetes.list"

# install docker before run follow
sudo apt-get update
sudo apt-get install -y kubelet kubeadm kubectl kubernetes-cni
sudo usermod -aG docker ssohjiro

sudo kubeadm init --apiserver-advertise-address=${self.network_interface.0.access_config.0.assigned_nat_ip}
sudo cp /etc/kubernetes/admin.conf $HOME/
sudo chown $(id -u):$(id -g) $HOME/admin.conf
SCRIPT
    destination = "/home/ssohjiro/startup-script.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "bash startup-script.sh"
    ]
  }
  provisioner "local-exec" {
    command = "echo ----------------------------------- setting ---------------------------------------\n"
  }
  provisioner "local-exec" {
    command = "echo scp ssohjiro@${self.network_interface.0.access_config.0.assigned_nat_ip}:/home/ssohjiro/admin.conf admin.conf"
  }
  provisioner "local-exec" {
    command = "echo export KUBECONFIG=$PWD/admin.conf" 
  }
  provisioner "local-exec" {
    command = "echo sudo kubeadm join --token _token_ ${self.network_interface.0.address}:6443"
  }
}

resource "google_compute_instance_template" "worker" {
  name         = "worker"
  machine_type = "n1-standard-1"
  can_ip_forward = false
  tags = ["worker"]
  instance_description = "description assigned to instances"
  disk {
    source_image = "ubuntu-1604-xenial-v20170619a"
    disk_size_gb = 20
    # type = "local-ssd"
    # auto_delete = false
    # boot = false
  }
  network_interface {
    network = "${google_compute_network.default.name}"
    access_config {
      // Ephemeral IP
    }
  }
  metadata {
    foo = "bar"
    "ssh-keys" = "ssohjiro:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDo7fIa/pjvxrBe1wUR+BR2aoXjbZjB8UvJmy9EgdjLN9TOchJVEeimM+Xolr4X0ZEfzMUQq77nMWJAMd2BBUbi/KE4sB2khOdDfC9hncYBjMFc9g+WYJausDexF+PKZX7PvISwEAsOoXlxBovt3l1V6DoHDVaSYReSv2TBL6CaQC9PcLl+soAWkrFjOeSgUnrQsgPUXE+vUj37JY8qkqd/+sNatShTLnNYU9eV6CYpqsUMH3ccy+vBn+55dra6zXAyO40WSZfTB5Q9FwrG5QrLuk8lNr4V5LzrOBBo4Cz0epIEm9xzmMHGzHhP1J61z1qwOuHPAL0A4rUjBknJ5qzD ssohjiro@2012MidMPR"
  }
  service_account {
    scopes = ["userinfo-email", "compute-ro", "storage-ro"]
  }
  metadata_startup_script = <<SCRIPT
#! /bin/bash
echo $USER
curl -fsSL https://get.docker.com/ | sh

apt-get install -y apt-transport-https
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
bash -c "echo deb http://apt.kubernetes.io/ kubernetes-xenial main >> /etc/apt/sources.list.d/kubernetes.list"

# install docker before run follow
apt-get update
apt-get install -y kubelet kubeadm kubectl kubernetes-cni
usermod -aG docker ssohjiro
SCRIPT
}

resource "google_compute_target_pool" "default" {
  name = "test"
  instances = [
    "asia-east1-a/worker",
  ]
  health_checks = [
    "${google_compute_http_health_check.default.name}",
  ]
}

resource "google_compute_instance_group_manager" "worker" {
  name = "terraform-test"
  description = "Terraform instance group manager for kube-worker"
  instance_template = "${google_compute_instance_template.worker.self_link}"
  base_instance_name = "kube-worker"
  zone = "asia-east1-a"
  target_size = 3
  update_strategy = "NONE"
  target_pools = ["${google_compute_target_pool.default.self_link}"]
  named_port {
    name = "customhttp"
    port = 8888
  }
  
  /*
  depends_on = [
    "google_compute_instance.master",
  ]
  */
}

resource "google_compute_http_health_check" "default" {
  name         = "test"
  request_path = "/health_check"
  timeout_sec        = 1
  check_interval_sec = 1
}

